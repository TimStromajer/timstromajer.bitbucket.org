
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    	var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
             "&password=" + encodeURIComponent(password),
    	async: false
    });
    
    return response.responseJSON.sessionId;
}


var ehri = [];
/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	var ehrId = "";
	var datumi = [["2003-01-02", "2005-01-02", "2007-10-10", "2007-03-07", "2008-07-10"], ["2001-01-29", "2002-01-11", "2003-10-22", "2004-03-30", "2005-07-12"], ["2003-01-02", "2005-01-02", "2007-10-10", "2007-03-07", "2008-07-10"]];
	var nasicenosti = [[78, 76, 90, 98, 92], [98, 90, 90, 97, 78], [60, 76, 99, 98, 99]];
  // TODO: Potrebno implementirati
	sessionId = getSessionId();
	var ime, priimek, datumRojstva;
    if(stPacienta == 1) {
    	ime = "Janez";
    	priimek = "Novak";
    	datumRojstva = "2000-01-01T00:00:00.000Z";
    } else if(stPacienta == 2) {
    	ime = "Majda";
    	priimek = "Mejdun";
    	datumRojstva = "1970-11-05T00:00:00.000Z";
    } else {
    	ime = "Bogdan";
    	priimek = "Bogdanovič";
    	datumRojstva = "1989-06-10T00:00:00.000Z";
    }

	
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        ehri[stPacienta-1] = ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		            if (party.action == 'CREATE') {
		                $("#kreirajSporocilo").html("<span class='obvestilo " +
                        "label label-success fade-in'>Uspešno kreiran EHR '" +
                        ehrId + "'.</span>");
		            	//$("#preberiEHRid").val(ehrId);
		            }
		            ehri[stPacienta-1] = ehrId;
		        },
		        error: function(err) {
		            $("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                	JSON.parse(err.responseText).userMessage + "'!");
	            }
	        });
	    }
	});
	
	setTimeout(function() {
		for(var i = 0; i<5; i++) {
		ehrId = ehri[stPacienta-1];
			var datumInUra = datumi[stPacienta-1][i];
			var nasicenostKrviSKisikom = nasicenosti[stPacienta-1][i];
			var merilec = "Doktor Franc";

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		console.log(datumInUra + " " + nasicenostKrviSKisikom + " " + ehrId);
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		      
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
		}
	}, 200);
	
	
	setTimeout(function(){
		//console.log("ehr je (" + ehri[stPacienta-1] + ") ");
	return ehrId;	
	}, 100);
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";
	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		            
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
	//console.log(user.name);
}

function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	//var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	//var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	//var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	//var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	//var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    //"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    //"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	//"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    //"vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    //"vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    //"vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
		
	}
}

function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	//var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 ) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'Koncentracijo kisika v krvi'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "spO2",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    			"table-hover'><tr><th>Datum in ura</th>" +
                    			"<th class='text-right'>spO2</th></tr>";
                    			var cas;
                    			var kisik;
                    			var stevec = 0;
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                        			"</td><td class='text-right'>" + res[i].spO2 +
                        			"</td></tr>";
                        			if(res[i].spO2 < 95 && stevec==0) {
                        				stevec = 1;
                        				cas = res[i].time;
                        				kisik = res[i].spO2;
                        				nizkaRaven(cas, kisik);
                        			}
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
						        narisiGraf(res);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
	$("#povzrocitelji").html("<div class='panel-group'>" + 
		"<div class='panel panel-default'>" +
		"<div class='panel-heading'>" +
		"<a data-toggle='collapse' href='#collapse6'>Kaj lahko povzroči zmanjšanje stopnje kisika v krvi</a></div>" +
		"<div id='collapse6' class='panel-collapse collapse'>" +
		" <ul class='list-group'>" +
		"<li class='list-group-item'><a href='https://en.wikipedia.org/wiki/Chronic_obstructive_pulmonary_disease'>COPD</a></li><li class='list-group-item'><a href='https://en.wikipedia.org/wiki/Acute_respiratory_distress_syndrome'>acute respiratory distress syndrome</a></li><li class='list-group-item'><a href='https://sl.wikipedia.org/wiki/Astma'>astma</a></li><li class='list-group-item'><a href='https://en.wikipedia.org/wiki/Pneumothorax'>collapsed lung</a></li>" +
		"<li class='list-group-item'><a href='https://en.wikipedia.org/wiki/Anemia'>anemia</a></li><li class='list-group-item'><a href='https://en.wikipedia.org/wiki/Congenital_heart_defect'>congenital heart defects</a></li><li class='list-group-item'><a href='https://en.wikipedia.org/wiki/Cardiovascular_disease'>heart disease</a></li><li class='list-group-item'><a href='https://en.wikipedia.org/wiki/Pulmonary_embolism'>pulmonary embolism</a></li></ul>" +
		 "</div></div>");
}

function nizkaRaven(cas, kisik) {
	document.getElementById("opozorilo").innerHTML = "dne " + cas + " ste imeli nizko raven kisika v krvi, kar lahko povzroči naslednje simptome:";
	var x = document.getElementById("rezultati");
	x.style.display = "none";
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function narisiGraf(res) {
	var dataPoints = [];
    var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2",
	title:{
		text: "Koncentracija kisika v krvi"
	},
	axisY:{
		includeZero: false
	},
	data: [{        
		type: "line",       
		dataPoints: dataPoints
	}]
	});
	function addData(data) {
			for(var i in data) {
				dataPoints.push({x: i, y: data[i].spO2});
			}
		chart.render();
	}
	
	addData(res);
}

function generacija() {
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
	setTimeout(function(){
		document.getElementById('preberiObstojeciEHR').options[1].value = ehri[0];
		document.getElementById('preberiObstojeciEHR').options[2].value = ehri[1];
		document.getElementById('preberiObstojeciEHR').options[3].value = ehri[2];
		
		document.getElementById('preberiEhrIdZaVitalneZnake').options[1].value = ehri[0];
		document.getElementById('preberiEhrIdZaVitalneZnake').options[2].value = ehri[1];
		document.getElementById('preberiEhrIdZaVitalneZnake').options[3].value = ehri[2];
	}, 150);
}


$(document).ready(function(){
	var nextMessageId = 0;
	var user = {id: 33, name: "Gost"};
	
	$('#preberiPredlogoBolnika').change(function() {
    	$("#kreirajSporocilo").html("");
    	var podatki = $(this).val().split(",");
    	$("#kreirajIme").val(podatki[0]);
    	$("#kreirajPriimek").val(podatki[1]);
    	$("#kreirajDatumRojstva").val(podatki[2]);
	});
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
    $("input").focus(function(){
        $(this).css("background-color", "#e6ffe6");
    });
    $("input").blur(function(){
        $(this).css("background-color", "#ffffff");
    });
    function updateChat() {
    	$.ajaxSetup({
		    headers: ""
		});
	    $.ajax({
	    	url: "https://teaching.lavbic.net/OIS/chat/api/messages/Skedenj/" + nextMessageId,
	    	type: 'GET',
	    	success: function(data) {
	    		for (var i in data) {
	    			var message = data[i];
	    			$("#test").append(" \
	    				<li class='media'> \
	    					<div class='media.body'> \
	    						<div class='media'> \
	    							<div class='media-body'> \
	    								<small class='text-muted'>" +message.user.name + ": " + message.text + " (" + message.time + ") \
	    							</div> \
	    						 </div> \
	    					</div> \
	    				</li>");
	    			nextMessageId = message.id + 1;
	    		}
	    		setTimeout(function() {updateChat()} , 5000);
	    	}
	    	
	    });
	}
    updateChat();

	function sendMessage() {
		$.ajaxSetup({
		    headers: ""
		});
    	$.ajax({
    		url: "https://teaching.lavbic.net/OIS/chat/api/messages/Skedenj",
    		type: "POST",
    		contentType: 'application/json',
    		data: JSON.stringify({user : user, text : $("#message").val()}),
    		success: function (data) {
    			$("#message").val("");
    		}
    	});
	}
    $("#send").click(sendMessage);
});
